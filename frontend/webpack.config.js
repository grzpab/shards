const ENVIRONMENTS = [
    "development",
    "production"
];

function getEnvironment(){
    const shardsEnv = process.env.SHARDS_ENVIRONMENT;

    if (shardsEnv) {
        if (ENVIRONMENTS.indexOf(shardsEnv) === -1) {
            throw new Error("Unsupported SHARDS_ENVIRONMENT variable");
        }

        return shardsEnv;
    }

    throw new Error("State the SHARDS_ENVIRONMENT variable");
}

module.exports = function getConfigByEnvironment(){

    const environment = getEnvironment();
    const getConfig = require(`./config/${environment}/webpack.js`);

    return getConfig(__dirname);
};
