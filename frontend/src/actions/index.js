export const GeneralActionTypes = {
    RESET: "RESET"
};

export const SettingsActionTypes = {
    SET_SETTINGS: "SET_SETTINGS"
};

export const CellsActionTypes = {
    CREATE_CELLS: "CREATE_CELLS",
    UPDATE_CELLS: "UPDATE_CELLS"
};

export const TurnActionTypes = {
    INCREASE_TURN_NUMBER: "INCREASE_TURN_NUMBER"
};
