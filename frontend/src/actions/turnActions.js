import { TurnActionTypes } from "./index.js";

export const increaseTurnNumber = () => ({
    type: TurnActionTypes.INCREASE_TURN_NUMBER,
});
