import { SettingsActionTypes } from "./index.js";

export const setSettings = (data) => ({
    type: SettingsActionTypes.SET_SETTINGS,
    data
});
