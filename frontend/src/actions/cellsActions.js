import { CellsActionTypes } from "./index.js";

export const createCells = (data) => ({
    type: CellsActionTypes.CREATE_CELLS,
    data
});

export const updateCells = (newCellsData) => ({
    type: CellsActionTypes.UPDATE_CELLS,
    newCellsData
});
