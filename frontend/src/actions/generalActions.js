import { GeneralActionTypes } from "./index.js";

export const reset = () => ({
    type: GeneralActionTypes.RESET
});
