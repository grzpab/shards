import React from "react";
import ReactDom from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import { applyMiddleware, compose, createStore } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
import AppContainer from "./containers/AppContainer.jsx";

const appElement = document.getElementById("shardsApp");

const store = createStore(
    rootReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
    window.__REDUX_DEVTOOLS_EXTENSION__(),
    compose(applyMiddleware(thunk))
);

ReactDom.render(
    <Provider store={ store }>
        <BrowserRouter>
            <Route exact path="/" component={AppContainer} />
        </BrowserRouter>
    </Provider>,
    appElement
);
