import { getRandomArbitrary } from "./functions.js";
import {
    LEVEL_TO_MIN_ATTACK_POINTS_FOR_HUMAN,
    LEVEL_TO_MAX_ATTACK_POINTS_FOR_HUMAN,
    LEVEL_TO_MIN_ATTACK_POINTS_FOR_COMPUTER,
    LEVEL_TO_MAX_ATTACK_POINTS_FOR_COMPUTER,
    LEVEL_TO_MIN_DEFENCE_POINTS_FOR_HUMAN,
    LEVEL_TO_MAX_DEFENCE_POINTS_FOR_HUMAN,
    LEVEL_TO_MIN_DEFENCE_POINTS_FOR_COMPUTER,
    LEVEL_TO_MAX_DEFENCE_POINTS_FOR_COMPUTER,

} from "./levels.js";
import { HUMAN, COMPUTER } from "./owners.js";
import { getVerticalNumberOfCells } from "./sizes.js";

export function createRandomCells(size, level) {
    const numberOfCellsPerDimension = getVerticalNumberOfCells(size);

    const cells = [];
    const updated = false;

    const getAttackPointsForHuman = () => getRandomArbitrary(
        LEVEL_TO_MIN_ATTACK_POINTS_FOR_HUMAN.get(level),
        LEVEL_TO_MAX_ATTACK_POINTS_FOR_HUMAN.get(level)
    );

    const getAttackPointsForComputer = () => getRandomArbitrary(
        LEVEL_TO_MIN_ATTACK_POINTS_FOR_COMPUTER.get(level),
        LEVEL_TO_MAX_ATTACK_POINTS_FOR_COMPUTER.get(level)
    );

    const getDefencePointsForHuman = () => getRandomArbitrary(
        LEVEL_TO_MIN_DEFENCE_POINTS_FOR_HUMAN.get(level),
        LEVEL_TO_MAX_DEFENCE_POINTS_FOR_HUMAN.get(level)
    );

    const getDefencePointsForComputer = () => getRandomArbitrary(
        LEVEL_TO_MIN_DEFENCE_POINTS_FOR_COMPUTER.get(level),
        LEVEL_TO_MAX_DEFENCE_POINTS_FOR_COMPUTER.get(level)
    );

    for (let y = 0; y < numberOfCellsPerDimension; y++) {
        for (let x = 0; x < numberOfCellsPerDimension * 2; x++) {
            const id = `${x}x${y}`;
            const owner = x < numberOfCellsPerDimension ? 1 : 0;
            const red = owner === HUMAN ? getAttackPointsForHuman() : 0;
            const blue = owner === COMPUTER ? getAttackPointsForComputer() : 0;
            const green = owner === HUMAN ? getDefencePointsForHuman() : getDefencePointsForComputer();

            cells.push({
                id, x, y, owner, red, green, blue, updated
            });
        }
    }

    return cells;
}
