export const BASIC_LEVEL = 0;
export const MEDIUM_LEVEL = 1;
export const ADVANCED_LEVEL = 2;

export const LEVELS = new Map([
    [BASIC_LEVEL, "Basic Level"],
    [MEDIUM_LEVEL, "Medium Level"],
    [ADVANCED_LEVEL, "Advanced Level"]
]);

export const LEVEL_TO_MIN_ATTACK_POINTS_FOR_HUMAN = new Map([
    [BASIC_LEVEL, 192], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 160], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 128] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MAX_ATTACK_POINTS_FOR_HUMAN = new Map([
    [BASIC_LEVEL, 255], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 192] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MIN_ATTACK_POINTS_FOR_COMPUTER = new Map([
    [BASIC_LEVEL, 128], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 128], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 128] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MAX_ATTACK_POINTS_FOR_COMPUTER = new Map([
    [BASIC_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 255] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MIN_DEFENCE_POINTS_FOR_HUMAN = new Map([
    [BASIC_LEVEL, 192], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 160], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 128] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MAX_DEFENCE_POINTS_FOR_HUMAN = new Map([
    [BASIC_LEVEL, 255], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 192] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MIN_DEFENCE_POINTS_FOR_COMPUTER = new Map([
    [BASIC_LEVEL, 128], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 128], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 128] // eslint-disable-line no-magic-numbers
]);

export const LEVEL_TO_MAX_DEFENCE_POINTS_FOR_COMPUTER = new Map([
    [BASIC_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [MEDIUM_LEVEL, 224], // eslint-disable-line no-magic-numbers
    [ADVANCED_LEVEL, 255] // eslint-disable-line no-magic-numbers
]);
