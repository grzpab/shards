export const S_SIZE = 0;
export const M_SIZE = 1;
export const L_SIZE = 2;

export const SIZES = new Map([
    [S_SIZE, "3x3"],
    [M_SIZE, "6x6"],
    [L_SIZE, "9x9"]
]);

export function getVerticalNumberOfCells(size) {
    return (size + 1) * 3; // eslint-disable-line no-magic-numbers
}

export function getHorizontalNumberOfCells(size) {
    return getVerticalNumberOfCells(size) * 2;
}
