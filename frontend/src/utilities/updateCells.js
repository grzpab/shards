import { HUMAN, COMPUTER } from "./owners.js";
import { MAX_NUMBER_OF_POINTS } from "./constants";

export function getCellsForUpdate(cells, selectedCell) {
    const attackPoints = selectedCell.owner === HUMAN ? selectedCell.red : selectedCell.blue;

    const neighbouringCells = cells.filter(cell =>
        Math.abs(cell.x - selectedCell.x) <= 1 &&
        Math.abs(cell.y - selectedCell.y) <= 1
    );

    const foeCells = neighbouringCells.filter(cell => cell.owner !== selectedCell.owner);

    if (foeCells.length) {
        const newFoeCells = foeCells.map(cell => {
            const prevalencePoints = cell.green - attackPoints;

            if (prevalencePoints <= 0 && selectedCell.owner === HUMAN) {
                return Object.assign({}, cell, {
                    owner  : HUMAN,
                    red    : cell.blue, // ATK
                    green  : selectedCell.green, // DEF
                    blue   : 0,
                    updated: true
                });
            }

            if (prevalencePoints <= 0 && selectedCell.owner === COMPUTER) {
                return Object.assign({}, cell, {
                    owner  : COMPUTER,
                    red    : 0,
                    green  : selectedCell.green, // DEF
                    blue   : cell.red, // ATK
                    updated: true
                });
            }

            return Object.assign({}, cell, {
                green  : prevalencePoints,
                updated: true
            });
        });

        const myCells = neighbouringCells.filter(cell => cell.owner === selectedCell.owner);

        return [].concat(newFoeCells, myCells);
    }

    const red = Math.floor(selectedCell.red / neighbouringCells.length);
    const green = Math.floor(selectedCell.green / neighbouringCells.length);
    const blue = Math.floor(selectedCell.blue / neighbouringCells.length);

    return neighbouringCells.map(cell => {
        if (cell.x === selectedCell.x && cell.y === selectedCell.y) {
            return Object.assign({}, cell, {
                red,
                green,
                blue,
                updated: true
            });
        }

        return Object.assign({}, cell, {
            red    : Math.min(cell.red + red, MAX_NUMBER_OF_POINTS),
            green  : Math.min(cell.green + green, MAX_NUMBER_OF_POINTS),
            blue   : Math.min(cell.blue + blue, MAX_NUMBER_OF_POINTS),
            updated: true
        });
    });
}

export function getAllOwnedCells(cells, owner) {
    return cells.filter(cell => cell.owner === owner);
}
