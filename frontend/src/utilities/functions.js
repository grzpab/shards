export function getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

export function getHex(n) {
    return n.toString(16).padStart(2, "0");
}

export function getContrast(red, green, blue) {
    return 0.299 * red + 0.587 * green + 0.114 * blue > 127; // eslint-disable-line no-magic-numbers
}

export function getContrastColor(red, green, blue) {
    return getContrast(red, green, blue) ? "#000000" : "#FFFFFF";
}
