export const DEBOUNCE_MS = 100;
export const MAX_NUMBER_OF_POINTS = 255;
export const CELL_BORDER_RADIUS_DIVIDER = 8;
