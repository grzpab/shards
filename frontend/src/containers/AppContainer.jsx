import { connect } from "react-redux";
import AppComponent from "../components/AppComponent.jsx";

const mapStateToProps = ({ settings }) => ({ settings });

const AppContainer = connect(mapStateToProps)(AppComponent);

export default AppContainer;
