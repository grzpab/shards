import { connect } from "react-redux";
import GameComponent from "../components/GameComponent.jsx";
import { reset } from "../actions/generalActions.js";

const mapStateToProps = ({ settings }) => ({ settings });

const mapDispatchToProps = (dispatch) => ({
    reset: () => {
        dispatch(reset());
    }
});

const GameContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(GameComponent);

export default GameContainer;
