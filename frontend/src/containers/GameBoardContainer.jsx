import { connect } from "react-redux";
import GameBoardComponent from "../components/GameBoardComponent.jsx";
import { updateCells } from "../actions/cellsActions.js";
import { increaseTurnNumber } from "../actions/turnActions.js";

const mapStateToProps = ({ settings, cells, turnNumber }) => ({ settings, cells, turnNumber });

const mapDispatchToProps = (dispatch) => ({
    updateCells: (newCells) => {
        dispatch(updateCells(newCells));
        dispatch(increaseTurnNumber());
    }
});

const GameBoardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(GameBoardComponent);

export default GameBoardContainer;
