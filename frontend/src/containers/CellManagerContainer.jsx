import { connect } from "react-redux";
import CellManagerComponent from "../components/CellManagerComponent.jsx";

const mapStateToProps = ({ cells }) => ({ cells });

const CellManagerContainer = connect(mapStateToProps)(CellManagerComponent);

export default CellManagerContainer;
