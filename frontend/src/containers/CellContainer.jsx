import { connect } from "react-redux";
import CellComponent from "../components/CellComponent.jsx";
import { updateCells } from "../actions/cellsActions.js";
import { increaseTurnNumber } from "../actions/turnActions.js";

const mapStateToProps = ({ settings, cells }) => ({ settings, cells });

const mapDispatchToProps = (dispatch) => ({
    updateCells: (newCells) => {
        dispatch(updateCells(newCells));
        dispatch(increaseTurnNumber());
    }
});

const CellContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(CellComponent);

export default CellContainer;
