import { connect } from "react-redux";
import SettingsComponent from "../components/SettingsComponent.jsx";
import { setSettings } from "../actions/settingsActions.js";
import { createCells } from "../actions/cellsActions.js";
import { createRandomCells } from "../utilities/createCells.js";

const mapStateToProps = ({ settings }) => ({ settings });

const mapDispatchToProps = (dispatch) => ({
    setSettings: (data) => {
        dispatch(setSettings(data));
        dispatch(createCells(createRandomCells(data.size, data.level)));
    }
});

const SettingsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(SettingsComponent);

export default SettingsContainer;
