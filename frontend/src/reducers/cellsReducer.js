import { GeneralActionTypes, CellsActionTypes } from "../actions";

const initialState = null;

export default function cellsReducer(state = initialState, action) {
    switch (action.type) {
    case GeneralActionTypes.RESET:
        return initialState;
    case CellsActionTypes.CREATE_CELLS:
        return action.data;
    case CellsActionTypes.UPDATE_CELLS: {
        const newCellsMap = new Map(action.newCellsData
            .map(cell => [cell.id, Object.assign({}, cell, { updated: true })])
        );

        return state.map(cell => newCellsMap.has(cell.id)
            ? newCellsMap.get(cell.id)
            : Object.assign({}, cell, { updated: false })
        );
    }
    default:
        return state;
    }
}
