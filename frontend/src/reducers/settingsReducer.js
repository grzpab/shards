import { GeneralActionTypes, SettingsActionTypes } from "../actions";

const initialState = null;

export default function settingsReducer(state = initialState, action) {
    switch (action.type) {
    case GeneralActionTypes.RESET:
        return initialState;
    case SettingsActionTypes.SET_SETTINGS:
        return action.data;
    default:
        return state;
    }
}
