import { GeneralActionTypes, TurnActionTypes } from "../actions";

const initialState = 0;

export default function settingsReducer(state = initialState, action) {
    switch (action.type) {
    case GeneralActionTypes.RESET:
        return initialState;
    case TurnActionTypes.INCREASE_TURN_NUMBER:
        return state + 1;
    default:
        return state;
    }
}
