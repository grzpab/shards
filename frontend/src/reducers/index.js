import { combineReducers } from "redux";
import settingsReducer from "./settingsReducer.js";
import cellsReducer from "./cellsReducer";
import turnReducer from "./turnReducer";

const rootReducer = combineReducers({
    settings  : settingsReducer,
    cells     : cellsReducer,
    turnNumber: turnReducer
});

export default rootReducer;
