import React, { Component } from "react";
import PropTypes from "prop-types";
import IndicatorComponent, { IndicatorType } from "./IndicatorComponent.jsx";
import GameBoardStyles from "../styles/GameBoardStyles.css";
import { MAX_NUMBER_OF_POINTS } from "../utilities/constants.js";
import { HUMAN } from "../utilities/owners.js";

export default class OtherCellManagerComponent extends Component {
    static propTypes = {
        cell              : PropTypes.object.isRequired,
        cellRef           : PropTypes.object,
        destroyCellManager: PropTypes.func.isRequired
    }

    handleOnMouseEnter = () => {
        // eslint-disable-next-line no-invalid-this
        this.props.destroyCellManager();
    }

    handleOnMouseOver = () => {
        // eslint-disable-next-line no-invalid-this
        this.props.destroyCellManager();
    }

    render() {
        const { cell, cellRef } = this.props;
        const { red, green, blue, x, y, owner } = cell;

        if (!cellRef) {
            return null;
        }

        const rect = cellRef.getBoundingClientRect();

        const r = MAX_NUMBER_OF_POINTS - red;
        const g = MAX_NUMBER_OF_POINTS - green;
        const b = MAX_NUMBER_OF_POINTS - blue;

        const internalStyle = {
            position       : "absolute",
            left           : rect.left,
            top            : rect.top,
            height         : rect.height - 2, // 1px border now included
            width          : rect.width - 2, // 1px border now included
            backgroundColor: `rgba(${r},${g}, ${b}, 0.5)`,
            borderColor    : owner === HUMAN ? "red" : "blue"
        };

        return <div
            className={GameBoardStyles.cell}
            style={internalStyle}
            key={`cell_manager_${x}_${y}`}
            onMouseEnter={this.handleOnMouseEnter}
            onMouseOver={this.handleOnMouseOver}
        >
            <IndicatorComponent rect={rect} value={red} type={IndicatorType.ATTACK_POINTS} />
            <IndicatorComponent rect={rect} value={green} type={IndicatorType.DEFENCE_POINTS} />
        </div>;
    }
}
