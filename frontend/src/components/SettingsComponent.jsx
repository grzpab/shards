import React, { Component } from "react";
import PropTypes from "prop-types";
import SettingsStyles from "../styles/SettingsStyles.css";

import { S_SIZE, SIZES } from "../utilities/sizes.js";
import { BASIC_LEVEL, LEVELS } from "../utilities/levels.js";

export default class SettingsComponent extends Component {

    static propTypes = {
        settings   : PropTypes.object,
        setSettings: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);

        this.state = {
            name : "",
            level: BASIC_LEVEL,
            size : S_SIZE,
        };
    }

    // eslint-disable-next-line class-methods-use-this
    componentDidMount() {
        document.title = "Shards";
    }

    // eslint-disable-next-line class-methods-use-this
    componentDidUpdate() {
        document.title = "Shards";
    }

    handleChange = (event) => {
        const { target } = event;
        const { name, value } = target;

        // eslint-disable-next-line no-invalid-this
        this.setState({
            [ name ]: value
        });
    }

    handleSubmit = () => {
        // eslint-disable-next-line no-invalid-this
        const { name, level, size } = this.state;

        // eslint-disable-next-line no-invalid-this
        this.props.setSettings({
            name,
            level: parseInt(level, 10),
            size : parseInt(size, 10),
        });
    }

    render() {
        const { settings, settings__div, settings__input, settings__select } = SettingsStyles;
        const nameInput = <input
            name="name"
            type="text"
            value={this.state.name}
            onChange={this.handleChange}
            className={settings__input}
        />;

        const levelOptions = [...LEVELS].map(([key, label]) =>
            <option key={key} value={key}>{label}</option>
        );

        const levelInput = <select
            name="level"
            value={this.state.level}
            onChange={this.handleChange}
            className={settings__select}
        >
            {levelOptions}
        </select>;

        const sizeOptions = [...SIZES].map(([key, label]) =>
            <option key={key} value={key}>{label}</option>
        );

        const sizeInput = <select
            name="size"
            value={this.state.size}
            onChange={this.handleChange}
            className={settings__select}
        >
            {sizeOptions}
        </select>;

        const submit = <input
            type="button"
            value="Let's play!"
            className={settings__input}
            onClick={this.handleSubmit}
            disabled={!this.state.name}
        />;

        return <form>
            <div className={settings}>
                <div className={`${settings__div} ${SettingsStyles[ "settings__header" ]}`}><h1>Shards</h1></div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__name--label" ]}`}>Name</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__name--input" ]}`}>{nameInput}</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__size--label" ]}`}>Size</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__size--select" ]}`}>{sizeInput}</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__level--label" ]}`}>Level</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__level--select" ]}`}>{levelInput}</div>
                <div className={`${settings__div} ${SettingsStyles[ "settings__footer" ]}`}>{submit}</div>
            </div>
        </form>;
    }
}
