import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import GameStyles from "../styles/GameStyles.css";
import GameBoardContainer from "../containers/GameBoardContainer.jsx";

class GameComponent extends PureComponent {
    // eslint-disable-next-line no-invalid-this
    reset = () => this.props.reset();

    render() {
        return <div className={`${GameStyles.game}`}>
            <div className={GameStyles.game__header}>
                <div className={GameStyles.game_header__logo} onClick={this.reset}>
                    SHARDS
                </div>
                <div className={GameStyles.game_header__empty}>&nbsp;</div>
                <div className={GameStyles.game_header__reset} onClick={this.reset}>
                    RESET
                </div>
            </div>
            <div className={GameStyles.game__board}>
                <GameBoardContainer />
            </div>
            <div className={GameStyles.game__footer} />
        </div>;
    }
}

GameComponent.propTypes = {
    settings: PropTypes.object,
    reset   : PropTypes.func.isRequired,
};

export default GameComponent;
