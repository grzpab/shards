import React, { Component } from "react";
import PropTypes from "prop-types";
import MainCellManagerComponent from "./MainCellManagerComponent.jsx";
import OtherCellManagerComponent from "./OtherCellManagerComponent.jsx";
import { getCellsForUpdate } from "../utilities/updateCells.js";

export default class CellManagerComponent extends Component {
    static propTypes = {
        cells                  : PropTypes.array.isRequired,
        cell                   : PropTypes.object.isRequired,
        cellRef                : PropTypes.object.isRequired,
        getNeighbouringCellRefs: PropTypes.func.isRequired,
        destroyCellManager     : PropTypes.func.isRequired,
    }

    constructor(props) {
        super(props);

        this.cellsForUpdate = getCellsForUpdate(props.cells, props.cell);
    }

    render() {
        const { cell, cellRef, destroyCellManager } = this.props;

        if (!cellRef) {
            return null;
        }

        const { x, y } = cell;

        const cellRefs = this.props.getNeighbouringCellRefs(x, y);

        const cellDivs = this.cellsForUpdate.map(c => {
            const key = `cell_manager_${c.x}_${c.y}`;

            if (c.x === x && c.y === y) {
                return <MainCellManagerComponent
                    cell={c}
                    cellRef={cellRef}
                    destroyCellManager={destroyCellManager}
                    key={key}
                />;
            }

            return <OtherCellManagerComponent
                cell={c}
                cellRef={cellRefs.get(`${c.x}_${c.y}`)}
                destroyCellManager={destroyCellManager}
                key={key}
            />;
        });

        return <div>
            {cellDivs}
        </div>;
    }
}
