import React, { Component } from "react";
import PropTypes from "prop-types";
import IndicatorComponent, { IndicatorType } from "./IndicatorComponent.jsx";
import GameBoardStyles from "../styles/GameBoardStyles.css";
import { MAX_NUMBER_OF_POINTS } from "../utilities/constants.js";

export default class MainCellManagerComponent extends Component {
    static propTypes = {
        cell              : PropTypes.object.isRequired,
        cellRef           : PropTypes.object.isRequired,
        destroyCellManager: PropTypes.func.isRequired,
    }

    handleOnMouseOut = () => {
        // eslint-disable-next-line no-invalid-this
        this.props.destroyCellManager();
    }

    handleOnMouseLeave = () => {
        // eslint-disable-next-line no-invalid-this
        this.props.destroyCellManager();
    }

    render() {
        const { cell, cellRef } = this.props;
        const { red, green, blue, x, y } = cell;

        const rect = cellRef.getBoundingClientRect();

        const r = MAX_NUMBER_OF_POINTS - red;
        const g = MAX_NUMBER_OF_POINTS - green;
        const b = MAX_NUMBER_OF_POINTS - blue;

        const internalStyle = {
            position       : "absolute",
            left           : rect.left,
            top            : rect.top,
            height         : rect.height - 2, // 1px border now included
            width          : rect.width - 2, // 1px border now included
            backgroundColor: `rgba(${r},${g}, ${b}, 0.5)`
        };

        return <div
            className={GameBoardStyles.cell}
            style={internalStyle}
            key={`cell_manager_${x}_${y}`}
            onMouseLeave={this.handleOnMouseLeave}
            onMouseOut={this.handleOnMouseOut}
        >
            <IndicatorComponent rect={rect} value={red} type={IndicatorType.ATTACK_POINTS} />
            <IndicatorComponent rect={rect} value={green} type={IndicatorType.DEFENCE_POINTS} />
        </div>;
    }
}
