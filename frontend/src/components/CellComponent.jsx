import React, { Component } from "react";
import PropTypes from "prop-types";
import GameBoardStyles from "../styles/GameBoardStyles.css";
import { HUMAN } from "../utilities/owners.js";
import { getContrastColor } from "../utilities/functions.js";
import { getCellsForUpdate } from "../utilities/updateCells.js";
import CellManagerContainer from "../containers/CellManagerContainer.jsx";

export default class CellComponent extends Component {
    static propTypes = {
        settings               : PropTypes.object.isRequired,
        cells                  : PropTypes.array.isRequired,
        cell                   : PropTypes.object.isRequired,
        updateCells            : PropTypes.func.isRequired,
        setCellRef             : PropTypes.func.isRequired,
        getNeighbouringCellRefs: PropTypes.func.isRequired,
    }

    constructor(props, context) {
        super(props, context);

        this.cellRef = null;

        const { red, green, blue } = this.props.cell;

        this.state = {
            red, green, blue
        };
    }

    static getDerivedStateFromProps(nextProps) {
        const { red, green, blue } = nextProps.cell;

        return {
            red, green, blue
        };
    }

    handleOnClick = () => {
        // eslint-disable-next-line no-invalid-this
        const { cells, cell } = this.props;

        if (cell.owner !== HUMAN) {
            console.warn(`You do not own (${cell.x}, ${cell.y})`);

            return;
        }

        const newCells = getCellsForUpdate(cells, cell);

        if (newCells) {
            // eslint-disable-next-line no-invalid-this
            this.props.updateCells(newCells);
        }
    }

    // eslint-disable-next-line no-invalid-this
    handleOnMouseEnter = () => this.createCellManager();

    // eslint-disable-next-line no-invalid-this
    handleOnMouseOver = () => this.createCellManager();

    createCellManager = () => {
        // eslint-disable-next-line no-invalid-this
        if (this.props.cell.owner !== HUMAN) {
            return;
        }

        // eslint-disable-next-line no-invalid-this
        if (!this.state.focused) {
            // eslint-disable-next-line no-invalid-this
            const { red, blue } = this.props.cell;

            const value = red === 0 ? blue : red;

            // eslint-disable-next-line no-invalid-this
            this.setState({
                red    : value,
                green  : value,
                blue   : value,
                focused: true
            });
        }
    }

    destroyCellManager = () => {
        // eslint-disable-next-line no-invalid-this
        const { red, green, blue } = this.props.cell;

        // eslint-disable-next-line no-invalid-this
        this.setState({ red, green, blue, focused: false });
    }

    render() {
        const { cell } = this.props;
        const { red, green, blue } = this.state;

        const style = {
            "backgroundColor": `rgb(${red},${green},${blue})`,
            "color"          : getContrastColor(red, green, blue),
            "border"         : cell.updated ? "2px red dashed" : "1px black solid"
        };

        const ownerLabel = cell.owner ? this.props.settings.name : "Computer";
        const ownerLabelBold = <b>{ownerLabel}</b>;

        const attackPoints = cell.owner ? cell.red : cell.blue;
        const healthPoints = cell.green;

        const cellManager = this.state.focused && cell.owner === HUMAN && this.cellRef ?
            <CellManagerContainer
                cell={cell}
                cellRef={this.cellRef}
                getNeighbouringCellRefs={this.props.getNeighbouringCellRefs}
                destroyCellManager={this.destroyCellManager}
            />
            : null;

        return <div
            className={GameBoardStyles.cell}
            style={style}
            onClick={this.handleOnClick}
            onMouseOver={this.handleOnMouseOver}
            onMouseEnter={this.onMouseEnter}
            ref={(element) => {
                this.props.setCellRef(cell.x, cell.y, element);
                this.cellRef = element;
            }}
        >
            {cellManager}
            <p>
                {ownerLabelBold}<br />
                {attackPoints}:{healthPoints}<br /><br/>
                {cell.id}
            </p>
        </div>;
    }
}
