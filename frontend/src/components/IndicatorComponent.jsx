import React from "react";
import PropTypes from "prop-types";
import CellStyles from "../styles/CellStyles.css";
import { CELL_BORDER_RADIUS_DIVIDER, MAX_NUMBER_OF_POINTS } from "../utilities/constants.js";

export const IndicatorType = {
    ATTACK_POINTS : 1,
    DEFENCE_POINTS: 2
};

const IndicatorComponent = (props) => {
    const padding = Math.ceil(props.rect.height / CELL_BORDER_RADIUS_DIVIDER / 2);

    const higherComponentStyle = {
        top   : padding * 2,
        bottom: padding * 2,
        width : padding,
    };

    if (props.type === IndicatorType.ATTACK_POINTS) {
        higherComponentStyle.left = padding;
    }
    else if (props.type === IndicatorType.DEFENCE_POINTS) {
        higherComponentStyle.right = padding;
    }
    else {
        throw new Error("The indicator type was incorrect.");
    }

    const percentage = Math.floor(100 - 100 * props.value / MAX_NUMBER_OF_POINTS);

    const backgroundColor = props.type === IndicatorType.ATTACK_POINTS ?
        CellStyles.ATTACK_POINTS_INDICATOR__BACKGROUND_COLOR :
        CellStyles.DEFENCE_POINTS_INDICATOR__BACKGROUND_COLOR;

    const lowerComponentStyle = {
        top: `${percentage}%`,
        backgroundColor
    };

    return <div
        className={CellStyles.higherIndicator}
        style={higherComponentStyle}
    >
        <div
            className={CellStyles.lowerIndicator}
            style={lowerComponentStyle}
        / >
    </div>;
};

IndicatorComponent.propTypes = {
    rect : PropTypes.object.isRequired,
    value: PropTypes.number.isRequired,
    type : PropTypes.number.isRequired
};

export default IndicatorComponent;
