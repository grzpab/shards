import React, { Component } from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";
import CustomProperties from "react-custom-properties";
import AppStyles from "../styles/AppStyles.css"; // eslint-disable-line no-unused-vars
import SettingsContainer from "../containers/SettingsContainer.jsx";
import GameContainer from "../containers/GameContainer.jsx";
import { DEBOUNCE_MS, CELL_BORDER_RADIUS_DIVIDER } from "../utilities/constants.js";
import { getVerticalNumberOfCells } from "../utilities/sizes.js";

const getProperties = (settings) => {
    const width = document.body.offsetWidth;
    const height = document.body.offsetHeight;

    const params = [width / 2, height / 2];
    const result = width > height ? Math.max(...params) : Math.min(...params);

    const gameBoardSize = `${result}px`;

    const verticalNumberOfCells = settings ? getVerticalNumberOfCells(settings.size) : 0;

    const gameBoardGridTemplateRows = "1fr ".repeat(verticalNumberOfCells);
    const gameBoardGridTemplateColumns = gameBoardGridTemplateRows.repeat(2);

    const cellBorderRadius = Math.floor(width / verticalNumberOfCells / 2 / CELL_BORDER_RADIUS_DIVIDER);
    const resetButtonWidth = verticalNumberOfCells ? Math.floor(width / verticalNumberOfCells / 2) : 0;

    return {
        "--GAME_BOARD--SIZE"                 : gameBoardSize,
        "--GAME_BOARD--GRID_TEMPLATE_ROWS"   : gameBoardGridTemplateRows,
        "--GAME_BOARD--GRID_TEMPLATE_COLUMNS": gameBoardGridTemplateColumns,
        "--GAME_BOARD__CELL--BORDER_RADIUS"  : `${cellBorderRadius}px`,
        "--GAME_BOARD__RESET_BUTTON--WIDTH"  : `${resetButtonWidth}px`
    };
};

export default class AppComponent extends Component {

    static propTypes = {
        settings: PropTypes.object
    }

    constructor(props, context) {
        super(props, context);

        this.handleResizing = debounce(this.handleResizing, DEBOUNCE_MS);

        this.state = {
            properties: null,
        };
    }

    static getDerivedStateFromProps(nextProps) {
        return {
            properties: getProperties(nextProps.settings)
        };
    }

    componentDidMount() {
        window.addEventListener("resize", this.handleResizing);
        window.addEventListener("orientationchange", this.handleResizing);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.handleResizing);
        window.removeEventListener("orientationchange", this.handleResizing);
    }

    // eslint-disable-next-line no-invalid-this
    handleResizing = () => this.setState({
        // eslint-disable-next-line no-invalid-this
        properties: getProperties(this.props.settings),
    });

    render() {
        const { properties } = this.state;

        if (!properties) {
            return null;
        }

        const child = this.props.settings ? <GameContainer /> : <SettingsContainer />;

        return <CustomProperties properties={properties}>
            {child}
        </CustomProperties>;
    }
}
