import React, { Component } from "react";
import PropTypes from "prop-types";
import GameBoardStyles from "../styles/GameBoardStyles.css";
import CellContainer from "../containers/CellContainer.jsx";
import { getAllOwnedCells, getCellsForUpdate } from "../utilities/updateCells.js";
import { getRandomArbitrary } from "../utilities/functions.js";
import { HUMAN, COMPUTER } from "../utilities/owners.js";

const COMPUTER_TURN_TIMEOUT = 750;

export default class GameBoardComponent extends Component {
    static propTypes = {
        settings   : PropTypes.object.isRequired,
        cells      : PropTypes.array.isRequired,
        turnNumber : PropTypes.number.isRequired,
        updateCells: PropTypes.func.isRequired
    }

    constructor(props) {
        super(props);

        this.cellRefs = new Map();
        this.getNeighbouringCellRefs = this.getNeighbouringCellRefs.bind(this);

        this.state = {
            waitingForComputer: false,
            isFinished        : false,
            winner            : null
        };

        this.timeout = null;
    }

    componentDidMount() {
        document.title = `Shards: Turn ${this.props.turnNumber}`;
    }

    componentDidUpdate() {
        document.title = `Shards: Turn ${this.props.turnNumber}`;

        // TODO: for a more advanced version:
        // move it outside the component, connect to the store
        // and use requestIdleCallback
        if (this.state.waitingForComputer && !this.timeout) {
            const { cells } = this.props;
            const ownedCells = getAllOwnedCells(cells, COMPUTER);

            this.timeout = setTimeout(() => {
                const randomId = getRandomArbitrary(0, ownedCells.length - 1);

                const cell = ownedCells[ randomId ];
                const newCells = getCellsForUpdate(cells, cell);

                this.props.updateCells(newCells ? newCells : []);

                this.timeout = null;
            }, COMPUTER_TURN_TIMEOUT);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        const { cells } = nextProps;
        const ownedCells = getAllOwnedCells(cells, COMPUTER);

        if (ownedCells.length === 0 || ownedCells.length === cells.length) {
            return {
                waitingForComputer: false,
                isFinished        : true,
                winner            : ownedCells.length === 0 ? HUMAN : COMPUTER
            };
        }

        if (nextProps.turnNumber % 2 === 0) {
            return {
                waitingForComputer: false
            };
        }

        if (nextProps.turnNumber % 2 === 1 && !prevState.waitingForComputer) {
            return {
                waitingForComputer: true
            };
        }

        return null;
    }

    render() {
        const { waitingForComputer, isFinished, winner } = this.state;

        if (isFinished) {
            const verb = winner === HUMAN ? "won" : "lost";

            return <div className={GameBoardStyles.game_board__finished}>
                <h1>{`You have ${verb} in the ${this.props.turnNumber - 1}th turn!`}</h1>
            </div>;
        }

        const cells = this.props.cells.map(cell =>
            <CellContainer
                key={cell.id}
                cell={cell}
                setCellRef={this.setCellRef}
                getNeighbouringCellRefs={this.getNeighbouringCellRefs}
            />
        );

        const waitingForComputerComponent = waitingForComputer ?
            <div className={GameBoardStyles.game_board__waiting}>
                <h1>{"Waiting for Computer"}</h1>
            </div> : null;

        return <div className={GameBoardStyles.game_board}>
            {cells}
            {waitingForComputerComponent}
        </div>;
    }

    setCellRef = (x, y, ref) => {
        // eslint-disable-next-line no-invalid-this
        this.cellRefs.set(`${x}_${y}`, ref);
    }

    getNeighbouringCellRefs = (x, y) => {
        //this can easily be an array
        const cellRefs = new Map();

        for (let i = x - 1; i <= x + 1; i++) {
            for (let j = y - 1; j <= y + 1; j++) {
                if (i == x && j == y) {
                    continue;
                }

                const key = `${i}_${j}`;

                // eslint-disable-next-line no-invalid-this
                if (this.cellRefs.has(key)) {
                    // eslint-disable-next-line no-invalid-this
                    cellRefs.set(key, this.cellRefs.get(key));
                }
            }
        }

        return cellRefs;
    }
}
