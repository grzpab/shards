const path = require("path");

module.exports = function defaultWebpackConfig(rootPath){
    const BUILD_PATH = path.resolve(rootPath, "build");
    const SOURCE_PATH = path.resolve(rootPath, "src");

    const cssTest = "css-loader?modules&importLoader=1&\
    localIdentName=[name]__[local]__[hash:base64:5]";

    return {
        context: SOURCE_PATH,
        output : {
            path      : BUILD_PATH,
            filename  : "bundle.js",
            publicPath: "/",
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    use : [
                        "babel-loader",
                    ],
                    exclude: /node_modules/
                },
                {
                    test  : /\.woff$/,
                    loader: "url-loader?mimetype=application/font-woff"
                },
                {
                    test: /\.(ttf|oet)$/,
                    use : ["url-loader"]
                },
                {
                    test: /\.scss$/,
                    use : [
                        "style-loader",
                        "css-loader",
                        "sass-loader"
                    ]
                },
                {
                    test: /\.gif$|.png$/,
                    use : ["url-loader"]
                },
                {
                    test: /\.css$/,
                    use : [
                        "style-loader",
                        cssTest,
                    ]
                }
            ]
        },
        externals: {
            "react/addons"                  : true,
            "react/lib/ExecutionEnvironment": true,
            "react/lib/ReactContext"        : true
        }
    };
};
