const path = require("path");
const webpack = require("webpack");
const webpackMerge = require("webpack-merge");

const defaultWebpackConfig = require("../defaultWebpackConfig.js");

module.exports = function getConfig(rootPath){
    const PROTOCOL = "http";
    const HOST = "0.0.0.0";
    const PORT = 8080;

    const URL = `${PROTOCOL}://${HOST}:${PORT}`;

    const BUILD_PATH = path.resolve(rootPath, "build");

    const config = {
        entry: [
            "react-hot-loader/patch",
            `webpack-dev-server/client?${URL}`,
            "webpack/hot/only-dev-server",
            "babel-polyfill",
            "./index.js"
        ],
        devtool  : "inline-source-map",
        devServer: {
            inline            : true,
            hot               : true,
            host              : HOST,
            port              : PORT,
            contentBase       : BUILD_PATH,
            historyApiFallback: true,
            publicPath        : "/",
            clientLogLevel    : "info"
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NamedModulesPlugin()
        ],
        mode: "development",
    };

    return webpackMerge(
        defaultWebpackConfig(rootPath),
        config
    );
};
