const webpackMerge = require("webpack-merge");

const defaultWebpackConfig = require("../defaultWebpackConfig.js");

module.exports = function getConfig(rootPath){
    const config = {
        entry: [
            "babel-polyfill",
            "./index.js"
        ],
        mode: "production",
    };

    return webpackMerge(
        defaultWebpackConfig(rootPath),
        config
    );
};
