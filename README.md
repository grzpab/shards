# Shards

by Gregory P. Pabian (_grzpab_)

## Description
A simple turn-based game demonstrating the current capabilities of the state-of-the-art frontend technologies.

## How to run it

    cd shards
    docker-compose up --build

After ensuring that the `webpack-dev-server` is running, navigate a modern browser of your choice to `0.0.0.0:8080`.

## How to run it in the production mode

    cd shards
    docker-compose -f docker-compose.prod.yml up --build

After ensuring that the `http-server` is running, navigate a modern browser of your choice to `0.0.0.0:8080`.

## Technological Stack
- Docker
- NPM (Node Package Manager)
- Babel
- webpack
- ES6
- React
- Redux (react-redux and react-redux-thunk)
- CSS3
- CSS Modules

## Rules
Before you start, please select:
- your name,
- the size of your part of the board (_3x3_, _6x6_, _9x9_)- you will be granted a left part of the board, and your opponent will play on the right side,
- difficulty: _basic_, _medium_, _advanced_.

### Shards
Each player will receive shards with his/her name on it, each one having a random number of attack (ATK) and defence (DEF) points (i.e. `200x100`). The exact range for those points is defined by the difficulty (the higher the level, the discrepancies between strength of players' tiles are greater, obviously). The minimum amount of points is 0, the maximum is 255.

For the human player, the color of each shards is calculated as `RGB(ATK, DEF, 0)` which creates a spectrum of red colours. For the computer player, the color is determined as `RGB(0, DEF, ATK)` which creates a spectrum of blue colours.

When a player clicks on its shard, it will trigger either an attack on foe's shards or a shard redistribution.

### Attack Phase
If a selected shard (invader) is neighbouring some enemy shards (defender), the following things will take place:
- if the invader's ATK is greater than defender's DEF, then the defender will be taken over by the attacking player: defender's ATK will be the absolute value of the difference (`abs(invader_ATK - defender_DEF)`) but the DEF will stay the same
- otherwise, defender's DEF will be reduced by the difference (damage)

### Redistribution Phase
If a selected shards has no foe shards around, its ATK and DEF will be redistributed equally between the surrounding shards, including the selected one. If the additional points added to a neighbouring shard exceed the maximum amount of points, then such a shard will get the maximum amount of points available.

## Turns
After one player has finished its turn, the other starts the new one. The game ends when someone has taken the shards from its adversary. Before you make a choice, you can see how it will affect the board by hovering your mouse over a desired shard. A overlay will open with the new values of ATK and DEF for each affected cells.
The effect of the last turn of the computer player are dotted red.
